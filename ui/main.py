from tkinter import *

class ntspyUI(object):

    def __init__(self):
        self.main_window = Tk()
        self.main_window.title('NTSPy')
        message_section = self._generate_message_section(self.main_window)
    
    def _generate_message_section(self, master):
        f_message_section = Frame(master=master)
        f_message_section.grid(row=1, column=1, sticky='nesw')

        # region Header
        f_header = LabelFrame(text="Header",master=f_message_section)
        f_header.grid(row=1, column=1, columnspan=2, padx=2)

        ## Number Entry
        l_number = Label(text='Number', master=f_header)
        l_number.grid(row=1, column=1, padx=2)

        e_number = Entry(master=f_header,width=15)
        e_number.grid(row=2, column=1, padx=2)

        ## Precedence Entry
        l_precedence = Label(text="Precedence", master=f_header)
        l_precedence.grid(row=1,  column=2, padx=2)

        e_precedence = Entry(master=f_header, width=15)
        e_precedence.grid(row=2, column=2, padx=2)

        ## Handling Entry
        l_handling = Label(text="Handling", master=f_header)
        l_handling.grid(row=1, column=3, padx=2)

        e_handling = Entry(master=f_header, width=15)
        e_handling.grid(row=2, column=3, padx=2)

        ## Station of Origin Entry
        l_orig_call = Label(text="Origin Call", master=f_header)
        l_orig_call.grid(row=1, column=4, padx=2)

        e_orig_call = Entry(master=f_header, width=10)
        e_orig_call.grid(row=2, column=4, padx=2)

        ## Check Entry
        l_check = Label(text="Check", master=f_header)
        l_check.grid(row=1, column=5, padx=2)

        e_check = Entry(master=f_header, width=7)
        e_check.grid(row=2, column=5, padx=2)

        ## Place of Origin Entry
        l_orig_place = Label(text="Place of Origin", master=f_header)
        l_orig_place.grid(row=1, column=6, padx=2)

        e_orig_place = Entry(master=f_header, width=25)
        e_orig_place.grid(row=2, column=6, padx=2)

        ## Time Filed Entry
        l_time_filed = Label(text="Time Filed", master=f_header)
        l_time_filed.grid(row=1, column=7, padx=2)

        e_time_filed = Entry(master=f_header, width=10)
        e_time_filed.grid(row=2, column=7, padx=2)

        ## Date Filed Entry
        l_date_filed = Label(text="Date Filed", master=f_header)
        l_date_filed.grid(row=1, column=7, padx=2)

        e_date_filed = Entry(master=f_header, width=25)
        e_date_filed.grid(row=2, column=7, padx=2)
        # endregion Header
        
        # region To
        f_to = LabelFrame(text="Recipient",master=f_message_section)
        f_to.grid(row=2, column=1, padx=2, sticky='nesw')

        ## Line 1 Entry
        e_to_line1 = Entry(master=f_to, width=50)
        e_to_line1.grid(row=1, column=1, columnspan=2, padx=2)

        ## Line 2 Entry
        e_to_line2 = Entry(master=f_to, width=50)
        e_to_line2.grid(row=2, column=1, columnspan=2, padx=2)

        ## Line 3 Entry
        e_to_line3 = Entry(master=f_to, width=50)
        e_to_line3.grid(row=3, column=1, columnspan=2, padx=2)

        ## Phone Entry
        l_to_phone = Label(master=f_to, text="Phone:")
        l_to_phone.grid(row=4, column=1)

        e_to_phone = Entry(master=f_to, width=40)
        e_to_phone.grid(row=4, column=2, padx=2, sticky='e')

        # endregion To

        # region RecievedBy
        f_recby = LabelFrame(text="Recieved By",master=f_message_section)
        f_recby.grid(row=2, column=2, padx=2, sticky='nesw')

        ## Station Entry
        l_rec_call = Label(master=f_recby, text="Callsign")
        l_rec_call.grid(row=1, column=1, padx=2)

        e_rec_call = Entry(master=f_recby, width=10)
        e_rec_call.grid(row=1, column=2, padx=2)

        ## Phone Entry
        l_rec_phone = Label(master=f_recby, text="Phone")
        l_rec_phone.grid(row=1, column=3, padx=2)

        e_rec_phone = Entry(master=f_recby, width=10)
        e_rec_phone.grid(row=1, column=4, padx=2)

        ## Name Entry
        l_rec_name = Label(master=f_recby, text="Name")
        l_rec_name.grid(row=2, column=1, padx=2)

        e_rec_name = Entry(master=f_recby, width=10)
        e_rec_name.grid(row=2, column=2, columnspan=3, padx=2, sticky='ew')

        ## Street Entry
        l_rec_street = Label(master=f_recby, text="Street Address")
        l_rec_street.grid(row=3, column=1, padx=2)

        e_rec_street = Entry(master=f_recby, width=10)
        e_rec_street.grid(row=3, column=2, columnspan=3, padx=2, sticky='ew')

        ## Location Entry
        l_rec_loc = Label(master=f_recby, text="City/State/Zip")
        l_rec_loc.grid(row=4, column=1, padx=2)

        e_rec_loc = Entry(master=f_recby, width=10)
        e_rec_loc.grid(row=4, column=2, columnspan=3, padx=2, sticky='ew')

        # endregion RecievedBy

        return f_message_section

    def show(self):
        self.main_window.mainloop()